const listURL = "https://www.siiir.edu.ro/siiir/list/budgetSchoolDTO.json";
const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
const listParam = "generatorKey=BGCONFIG&filter=%5B%7B%22property%22%3A%22finalized%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5Bfalse%5D%7D%2C%7B%22property%22%3A%22schoolYear.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B"+schoolYearId+"%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=2000";

const actionURL = "https://www.siiir.edu.ro/siiir/closeSchoolCost.json";
const actionParam = ["id=", "&schoolYearId=", "&configKey="];

//restore console
let iFrame = document.createElement('iframe');
iFrame.style.display = 'none';
document.body.appendChild(iFrame);
window.console = iFrame.contentWindow.console;

//IIFE to use async
(async() => {

    const schoolListResult = await fetch(
        listURL + "?_dc=" + (new Date).getTime(), {
            method: 'post',
            credentials: 'same-origin',
            headers: {
                "Accept": "application/json",
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                "X-Requested-With": "XMLHttpRequest"
            },
            body: listParam
        }
    );

    const schoolListJSON = await schoolListResult.json();


    for (const item of schoolListJSON.page.content) {
        if (!item.finalized) {
            await fetch(
                actionURL, {
                    method: 'post',
                    credentials: 'same-origin',
                    headers: {
                        "Accept": "application/json",
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                        "X-Requested-With": "XMLHttpRequest"
                    },
                    body: actionParam[0] +
                        item.internalId +
                        actionParam[1] +
                        schoolYearId +
                        actionParam[2] +
                        item.configKey
                }
            );
        }
    }
})();