const schoolYears = [10, 11, 20, 21]; //1:2013-2014, 2:2014-2015, 10:2015-2016, 11:2016-2017, 20:2017-2018, 21:2018-2019, 22:2019-2020, 23:2020-2021
const unitateListURL = "https://www.siiir.edu.ro/siiir/list/school.json";

//join with schoolYearId
const unitateListParams = ["generatorKey=APJSQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A", "%7D&page=1&start=0&limit=1000"];

const unitateBugetURL = "https://www.siiir.edu.ro/siiir/list/budget.json";

//join with schoolId, schoolYearId, schoolYearId (yes, twice)
const unitateBugetParams = ["filter=%5B%7B%22property%22%3A%22school.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B", "%5D%7D%2C%7B%22property%22%3A%22schoolYear.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B", "%5D%7D%5D&page=1&start=0&limit=10000&requestParams=%7B%22schoolYearId%22%3A", "%7D"];

const unitatePlatiURL = "https://www.siiir.edu.ro/siiir/list/payment.json";

//join with schoolId, schoolYearId, schoolYearId (yes, twice)
const unitatePlatiParams = ["filter=%5B%7B%22property%22%3A%22school.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B", "%5D%7D%2C%7B%22property%22%3A%22schoolYear.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B", "%5D%7D%5D&page=1&start=0&limit=10000&requestParams=%7B%22schoolYearId%22%3A", "%7D"];

const dataStructure = [{
        header: "An școlar",
        value: "schoolYear.code"
    }, {
        header: "CIF Unitate",
        value: "school.fiscalCode"
    }, {
        header: "Denumire Unitate",
        value: "school.description"
    }, {
        header: "Titlu",
        value: "article.code"
    }, {
        header: "Clasificație",
        value: "budgetAccount.code"
    },

    {
        header: "Tip Buget",
        value: "budgetType"
    }, //1 - buget inițial, 3-12? - influențe Inf4=6
    {
        header: "Buget Trim 1",
        value: "budgetValueTrim_1"
    }, {
        header: "Buget Trim 2",
        value: "budgetValueTrim_2"
    }, {
        header: "Buget Trim 3",
        value: "budgetValueTrim_3"
    }, {
        header: "Buget Trim 4",
        value: "budgetValueTrim_4"
    },

    {
        header: "Luna",
        value: "monthOfYear"
    }, {
        header: "Plăți cumulate",
        value: "cumulativeValue"
    }, {
        header: "Plăți aferente lunii",
        value: "monthlyValue"
    }
];

const emptycell = `,""`;

const data = [];

//restore console
let iFrame = document.createElement('iframe');
iFrame.style.display = 'none';
document.body.appendChild(iFrame);
window.console = iFrame.contentWindow.console;

//internal replaceAll
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

var saveAs=saveAs||function(e){"use strict";if(typeof e==="undefined"||typeof navigator!=="undefined"&&/MSIE [1-9]\./.test(navigator.userAgent)){return}var t=e.document,n=function(){return e.URL||e.webkitURL||e},r=t.createElementNS("http://www.w3.org/1999/xhtml","a"),o="download"in r,i=function(e){var t=new MouseEvent("click");e.dispatchEvent(t)},a=/constructor/i.test(e.HTMLElement),f=/CriOS\/[\d]+/.test(navigator.userAgent),u=function(t){(e.setImmediate||e.setTimeout)(function(){throw t},0)},d="application/octet-stream",s=1e3*40,c=function(e){var t=function(){if(typeof e==="string"){n().revokeObjectURL(e)}else{e.remove()}};setTimeout(t,s)},l=function(e,t,n){t=[].concat(t);var r=t.length;while(r--){var o=e["on"+t[r]];if(typeof o==="function"){try{o.call(e,n||e)}catch(i){u(i)}}}},p=function(e){if(/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(e.type)){return new Blob([String.fromCharCode(65279),e],{type:e.type})}return e},v=function(t,u,s){if(!s){t=p(t)}var v=this,w=t.type,m=w===d,y,h=function(){l(v,"writestart progress write writeend".split(" "))},S=function(){if((f||m&&a)&&e.FileReader){var r=new FileReader;r.onloadend=function(){var t=f?r.result:r.result.replace(/^data:[^;]*;/,"data:attachment/file;");var n=e.open(t,"_blank");if(!n)e.location.href=t;t=undefined;v.readyState=v.DONE;h()};r.readAsDataURL(t);v.readyState=v.INIT;return}if(!y){y=n().createObjectURL(t)}if(m){e.location.href=y}else{var o=e.open(y,"_blank");if(!o){e.location.href=y}}v.readyState=v.DONE;h();c(y)};v.readyState=v.INIT;if(o){y=n().createObjectURL(t);setTimeout(function(){r.href=y;r.download=u;i(r);h();c(y);v.readyState=v.DONE});return}S()},w=v.prototype,m=function(e,t,n){return new v(e,t||e.name||"download",n)};if(typeof navigator!=="undefined"&&navigator.msSaveOrOpenBlob){return function(e,t,n){t=t||e.name||"download";if(!n){e=p(e)}return navigator.msSaveOrOpenBlob(e,t)}}w.abort=function(){};w.readyState=w.INIT=0;w.WRITING=1;w.DONE=2;w.error=w.onwritestart=w.onprogress=w.onwrite=w.onabort=w.onerror=w.onwriteend=null;return m}(typeof self!=="undefined"&&self||typeof window!=="undefined"&&window||this.content);if(typeof module!=="undefined"&&module.exports){module.exports.saveAs=saveAs}else if(typeof define!=="undefined"&&define!==null&&define.amd!==null){define([],function(){return saveAs})}

//IIFE to get async
(async() => {

    //for each schoolyear, IN PARALLEL
    await Promise.all(
        schoolYears.map(
            async schoolYearId => {
                const schoolListResult = await fetch(
                    unitateListURL + "?_dc=" + (new Date).getTime(), {
                        method: 'post',
                        credentials: 'same-origin',
                        headers: {
                            "Accept": "application/json",
                            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                            "X-Requested-With": "XMLHttpRequest"
                        },
                        body: unitateListParams[0] + schoolYearId + unitateListParams[1]
                    }
                );
                const schoolListJSON = await schoolListResult.json();


                for (const school of schoolListJSON.page.content) {
                    //get buget
                    const bugetResult = await fetch(
                        unitateBugetURL + "?_dc=" + (new Date).getTime(), {
                            method: 'post',
                            credentials: 'same-origin',
                            headers: {
                                "Accept": "application/json",
                                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                                "X-Requested-With": "XMLHttpRequest"
                            },
                            body: unitateBugetParams[0] +
                                school.id +
                                unitateBugetParams[1] +
                                schoolYearId +
                                unitateBugetParams[2] +
                                schoolYearId +
                                unitateBugetParams[3]
                        }
                    );
                    const bugetJSON = await bugetResult.json();
                    bugetJSON.page.content.forEach(bugetItem => {
                        data.push(
                            dataStructure.reduce(
                                (dataLine, dataColumn) => {
                                    try {
                                        //just a fancier way to write value = bugetItem[dataColumn.value] that actually works
                                        const value = dataColumn.value.split('.').reduce((o, i) => o[i], bugetItem).toString().replaceAll(`"`, `""`);
                                        return dataLine + `,"${value}"`;
                                    } catch (err) {
                                        return dataLine + emptycell;
                                    }
                                },
                                `"BUGET"`
                            )
                        );
                    });
                    //get plati
                    const platiResult = await fetch(
                        unitatePlatiURL + "?_dc=" + (new Date).getTime(), {
                            method: 'post',
                            credentials: 'same-origin',
                            headers: {
                                "Accept": "application/json",
                                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                                "X-Requested-With": "XMLHttpRequest"
                            },
                            body: unitatePlatiParams[0] +
                                school.id +
                                unitatePlatiParams[1] +
                                schoolYearId +
                                unitatePlatiParams[2] +
                                schoolYearId +
                                unitatePlatiParams[3]
                        }
                    );
                    const platiJSON = await platiResult.json();
                    platiJSON.page.content.forEach(platiItem => {
                        data.push(
                            dataStructure.reduce(
                                (dataLine, dataColumn) => {
                                    try {
                                        //just a fancier way to write value = platiItem[dataColumn.value] that actually works
                                        const value = dataColumn.value.split('.').reduce((o, i) => o[i], platiItem).toString().replaceAll(`"`, `""`);
                                        return dataLine + `,"${value}"`;
                                    } catch (err) {
                                        return dataLine + emptycell;
                                    }
                                },
                                `"PLATI"`
                            )
                        );
                    });

                }
            })
    );

    const headers = dataStructure.map(item => item.header);

    const fileName = "export_buget_plati.csv";
    var buffer = "\uFEFF" + "TIP Dată," + headers.join(",") + "\n" + data.join("\n");
    var blob = new Blob([buffer], {
        "type": "text/csv;charset=utf8;"
    });
    saveAs(blob, fileName);
})();
