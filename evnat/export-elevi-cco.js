const listUrl = "https://evnat.edu.ro/evnat/list/candidateEvaluation.json";
const listLimit = 1000;
const ListParam =[
`generatorKey=CRACQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&viewName=appealRomanian&page=1&start=0&limit=${listLimit}`,
`generatorKey=CMAACQC&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&viewName=appealMaternal&page=1&start=0&limit=${listLimit}`,
`generatorKey=CMACQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&viewName=appealMath&page=1&start=0&limit=${listLimit}`
];

const csvData = [];

const headers = [
"CNP",
"First Name",
"I",
"Last Name",
"Centru Examen",
"Judet CE",
"Școala proveniență"
];
const values = [
"student.nin",
"student.firstName",
"student.fatherInitial",
"student.lastName",
"examCenter.description",
"examCenter.school.county.code",
"student.school.longName"
];

///IIFE for async
(async ()=>{
  for (const param of ListParam){
    const listResult = await fetch(listUrl + "?_dc=" + (new Date).getTime(), {
          method: 'post',
          credentials: 'same-origin',
          headers: {
            "Accept": "application/json",
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
          },
          body: param
        });
    const listJSON = await listResult.json();
    const view = listJSON.view;
    for (const item of listJSON.page.content){
      const oneLine = [];
      //parse and generate line, escape double quotes by doubling them according to CSV standard
      for (const value of values ){
        try {
          oneLine.push('"' + value.split('.').reduce((o, k) => o[k], item).toString().split('"').join('""') + '"');
        } catch(err) {
          console.log(err);
          oneLine.push('""');
        }
      }
     csvData.push(oneLine.join(','));
    }
  }

  const fileName = "export_elevi_cco.csv";
  const buffer = "\uFEFF" + headers.join(",") + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    "type": "text/csv;charset=utf8;"
  });
  
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement('a');
  a.href = url;
  a.download = fileName;
  a.click(); 
})();