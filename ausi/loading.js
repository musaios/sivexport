var loadingSvg = document.createElementNS('http://www.w3.org/2000/svg',"svg");
var mainPath = document.createElementNS('http://www.w3.org/2000/svg',"path");
var animation = document.createElementNS('http://www.w3.org/2000/svg',"animateTransform");

loadingSvg.setAttributeNS(null,"x","0px");
loadingSvg.setAttributeNS(null,"y","0px");
loadingSvg.setAttributeNS(null,"width","40px");
loadingSvg.setAttributeNS(null,"height","40px");
loadingSvg.setAttributeNS(null,"viewBox","0 0 50 50");
loadingSvg.setAttributeNS(null,"style","enable-background:new 0 0 50 50;");

mainPath.setAttributeNS(null,"fill","#000");
mainPath.setAttributeNS(null,"d","M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z");

animation.setAttributeNS(null,"attributeType","xml");
animation.setAttributeNS(null,"attributeName","transform");
animation.setAttributeNS(null,"type","rotate");
animation.setAttributeNS(null,"from","0 25 25");
animation.setAttributeNS(null,"to","360 25 25");
animation.setAttributeNS(null,"dur","0.6s");
animation.setAttributeNS(null,"repeatCount","indefinite");

mainPath.appendChild(animation);

loadingSvg.appendChild(mainPath);

