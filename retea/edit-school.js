(async ()=>{
//user config
//const schoolYears = [1,2,10,11,20,21,22,23];
const schoolYears = [11,20,21,22,23]; //[AMS.util.AppConfig.selectedYear.data.id];
//const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
const listUrl = "https://www.siiir.edu.ro/siiir/list/school.json";
const unsafeName = `Centrul Județean de Excelență Covasna`; //name to search for do NOT use 
const name = unsafeName.split('"').join('_');

//const listParamString = "generatorKey=ASQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A" + schoolYearId + "%7D";
//const listParamString = "generatorKey=ASQG&filter=%5B%7B%22property%22%3A%22longName%22%2C%22criteria%22%3A%22LIKE%22%2C%22parameters%22%3A%5B%22"+encodeURI(name)+"%22%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A"+schoolYearId+"%7D&page=1&start=0&limit=25";
const extraListUrl = "https://www.siiir.edu.ro/siiir/list/asocSchoolLevel.json";
const actionUrl = "https://www.siiir.edu.ro/siiir/management/school/" //dynamic id+dc+schoolYearId

//restore console
let iFrame = document.createElement('iframe');
iFrame.style.display = 'none';
document.body.appendChild(iFrame);
window.console = iFrame.contentWindow.console;

schoolYears.forEach(schoolYearId=>{
  const listParamString = "generatorKey=ASQG&filter=%5B%7B%22property%22%3A%22longName%22%2C%22criteria%22%3A%22LIKE%22%2C%22parameters%22%3A%5B%22"+encodeURI(name)+"%22%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A"+schoolYearId+"%7D&page=1&start=0&limit=25";
//iife - so we can use async
(async ()=>{
  const listResult = await fetch(listUrl + "?_dc=" + (new Date).getTime(), {
                method: 'post',
                credentials: 'same-origin',
                headers: {
                    "Accept": "application/json",
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
                body: listParamString
              });
  const listJSON = await listResult.json();
  const payload = listJSON.page.content[0];

  //if PJ we need to get schoollevels
  if (payload.parentSchool == null) {
    const schoolLevelsResult = await fetch(extraListUrl + "?_dc=" + (new Date).getTime(), {
                  method: 'post',
                  credentials: 'same-origin',
                  headers: {
                      "Accept": "application/json",
                      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                  },
                  body: "generatorKey=asocSchoolLevel_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22school.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B" + payload.id + "%5D%7D%5D&sort=%5B%7B%22property%22%3A%22level.orderBy%22%2C%22direction%22%3A%22ASC%22%7D%5D&page=1&start=0&limit=25&requestParams=%7B%22schoolYearId%22%3A" + schoolYearId + "%7D"
              });
    const schoolLevels = await schoolLevelsResult.json();
    payload.schoolLevels = schoolLevels.page.content;
  }

  //=========MODIFY PAYLOAD HERE===========

  //11349804

  //payload.postalCode="525400";
  payload.street="Kőrösi Csoma Sándor";
  payload.streetNumber="19";
  

  /*payload.postalCode="525300";
  payload.street="Gării";
  payload.streetNumber="1";*/

  //payload.longName = "Casa Corpului Didactic \"Csutak Vilmos\" Sfântu Gheorghe"
  //payload.operatingMode.id=2
  //payload.operatingMode.code="DOISCH"
  //payload.operatingMode.description="Două schimburi/zi"
  //payload.shortName = "LIC. EC. \"BERDE ÁRON\" SFÂNTU GHEORGHE"
  //payload.fiscalCode = "13650749";


  //payload.longName="Palatul Copiilor Sfântu Gheorghe"
  //payload.shortName="PC SFÂNTU GHEORGHE"
  //payload.operatingMode.id=1
  //payload.operatingMode.code="UNUSCH"
  //payload.operatingMode.description="Un schimb/zi"
  

  //=========END PAYLOAD MODIFY============

  const modResult = await fetch(actionUrl + "/" + payload.id + "?_dc=" + (new Date).getTime() + "&schoolYearId=" + schoolYearId, {
                  method: 'put',
                  credentials: 'same-origin',
                  headers: {
                      "Accept": "application/json",
                      "Content-type": "application/json"
                  },
                  body: JSON.stringify(payload)
              });

  const modResultJSON = await modResult.json();

  console.log("Modified: ", modResultJSON);

})();
});

})();
