//IIFE for async
(async() => {
    const pageLimit = 500;
    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
    const listUrl = "https://www.siiir.edu.ro/siiir/list/school.json"
    const listParams = `generatorKey=ASQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;
    const actionUrl = "https://www.siiir.edu.ro/siiir/applyModificationsToFutureSchool.json"
    const actionParamsPrefix = "id=";
    const actionParamsSuffix = "";

    //restore console
    let iFrame = document.createElement('iframe');
    iFrame.style.display = 'none';
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
        method: "post",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
        body: body,
    });


    //get nr of results
    const primingListResult = await siiirFetch(listUrl, listParams + "&limit=1&page=1&start=0");

    const primingListJSON = await primingListResult.json();

    const maxItems = primingListJSON.page.total;
    const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

    for (let page = 1; page <= maxPages; page++) {

        const start = (page - 1) * pageLimit;

        console.log(`Processing page ${page} of ${maxPages}`);

        const listResult = await siiirFetch(listUrl, listParams + `&limit=${pageLimit}&page=${page}&start=${start}`);

        const listJSON = await listResult.json();

        //do the actions
        for (const item of listJSON.page.content) {
            await siiirFetch(actionUrl, actionParamsPrefix + item.id + actionParamsSuffix);
            console.log("Processed: " + item.longName);
        }
        console.log("DONE!");
    }

})();