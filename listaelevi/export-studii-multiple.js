//IIFE for async
(async () => {
  const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
  const elevListUrl = "https://www.siiir.edu.ro/siiir/list/asocStudentAtSchoolView.json";
  const elevListParamPrefix = `generatorKey=ASSQG&filter=%5B%7B%22property%22%3A%22student.nin%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B%22`;
  const elevListParamSuffix = `%22%5D%7D%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D&page=1&start=0&limit=1&sort=%5B%7B%22property%22%3A%22from%22%2C%22direction%22%3A%22DESC%22%7D%5D`;

  const studiiUrl = "https://www.siiir.edu.ro/siiir/list/studentEducation.json";
  const studiiParamsPrefix = `generatorKey=studentEducation_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22student.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B`;
  const studiiParamsSuffix = `%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&limit=25`;

  //beware of spaces
  const elevCNPs = `1234567890123
1234567890123
1234567890123
1234567890123
1234567890123`;

  const csvData = [];

  const csvHeaders = [
    ["CNP", "student.nin"],
    ["Nume", "student.lastName"],
    ["Ini", "student.fatherInitial"],
    ["Prenume", "student.firstName"],
    ["Prenume1", "student.firstName1"],
    ["Prenume2", "student.firstName2"],
    ["ID Asoc", "id"],
    ["An școlar", "schoolYear.code"],
    ["Județ PJ","school.county.code"],
    ["Unitate PJ", "school.parentSchoolLongName"],
    ["Unitate formațiune", "school.longName"],
    ["Nivel", "level.code"],
    ["Clasă", "studyFormation.code"],
    ["Situație", "studyStatus.description"],
    ["Dela", "from"],
    ["Pânăla", "to"],
  ];


  //restore console
  let iFrame = document.createElement("iframe");
  iFrame.style.display = "none";
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
    method: "post",
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-type": "application/x-www-form-urlencoded",
    },
    body: body,
  });

  const CNPs = elevCNPs.split("\n");

  let currentCNP = 0;
  const maxCNP = CNPs.length;

  for (const CNP of CNPs) {
    currentCNP++;
    console.log(`Processing CNP ${currentCNP} of ${maxCNP}`);
    const CNPResult = await siiirFetch(elevListUrl, elevListParamPrefix + CNP + elevListParamSuffix);
    const CNPJSON = await CNPResult.json();
    if (CNPJSON.page.content.length > 0) {
      const elevId = CNPJSON.page.content[0].student.id;
      const studiiResult = await siiirFetch(studiiUrl, studiiParamsPrefix + elevId + studiiParamsSuffix);
      const studiiJSON = await studiiResult.json();

      for (const studii of studiiJSON.page.content) {
        const oneLine = [];
        for (const headerItem of csvHeaders) {
          // studii[key1.key2.key3] -> studii[key1][key2][key3] and " -> ""
          try {
            oneLine.push(headerItem[1].split('.').reduce((o, i) => o[i], studii).split('"').join('""'));
          } catch (err) {
            oneLine.push("");
          }
        }
        csvData.push(`"${oneLine.join(`","`)}"`);
      }
    }
  }

  const fileName = "export_studii.csv";
  const buffer = "\uFEFF" + csvHeaders.map(item => item[0]).join(",") + "\n" + csvData.join("\n");
  const blob = new Blob([buffer], {
    type: "text/csv;charset=utf8;",
  });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
}
)();