//IIFE for async
(async () => {
  const actionUrl =
    "https://www.siiir.edu.ro/siiir/management/entityAttributeValueStudent";

  const deleteActionUrl =
    "https://www.siiir.edu.ro/siiir/management/entityAttributeValueStudent/";

  const listUrl =
    "https://www.siiir.edu.ro/siiir/list/entityAttributeValueStudent.json";
  const listParamPrefix = `generatorKey=entityAttributeValueStudent_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22student.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B`;
  const listParamSuffix = `%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%7D&page=1&start=0&limit=25`;

  //1350 = CSS
  //1346 = palate
  //1348 = CEX
  //1345 = Suplimentar
  const attribValuesToGenerate = {
    "1348": {
      id: 1348,
      value: "DA",
    }
  };

  const attribIdArray = Object.keys(attribValuesToGenerate);

  //ID Elev = ultima coloană din raportul 400 - export complet elevi
  //atenție să nu fie linii în plus, respectați formatul de mai jos, un ID per linie fără linii goale.
  const elevIds = `152801
648301
671401
674151
680851
699101
700701
701401
704401
705101
707101
708201
708551
708801
710351
714351
714501
715401
715501
716251
716951
720801
721001
721401
721551
721651
761751
771251
771351
771501
774251
780601
784451
792501
795701
806801
810951
816651
820501
838551
900701
996151
1027751
1028351
1028901
1030201
1033251
1038351
1047251
1062751
1077851
1086151
1088201
1110801
1112001
1113451
1121051
1132101
1144101
1145551
1309401
1310651
1329751
1331101
1332001
1333451
1337501
1338101
1341051
1343051
1352151
1433251
1442251
1460051
1464651
1483951
1496351
1505701
1508951
1511501
1521801
1525601
1527901
1559801
1560301
1561201
1561751
1562051
1571651
1581451
1585251
1669251
1780851
1788001
1806701
1816901
1824101
1826451
1842901
1844901
1927051
1927851
1929301
1929551
1930951
1931301
1931501
1940851
1983401
1997701
2336301
2348301
2361151
2446501
2469951
2536051
2595651
2596151
2606801
2656701
2749651
2898651
2941001
2985101
2992301
3179801
3366301
3377151
6422501
6453351
6617151
6678401
6679201
6683151
6690601
6824251
6829651
6863351
7880657
7880713
7880727
7880776
7880791
7880902
7917419
7918086
7920255
7921513
7944478
7944521
8497385
8884040
9573348
9598920
9602549
9721824
9722132
9725070
9756579
9850116
9904044
9990204
9990344
10088414
10098754
10104579
10272067
10272972
10294904
10296250
10304768
10363599
10371715
10377021
10381917
10382026
10382337
10382474
10784685
10933523
11035792
11036063
11036171
11038703
11045532
11049268
11049385
11119129
11119186
11119800
11119820
11119866
11120013
11120039
11120549`;

  const attributeTemplate = {
    id: null,
    valueDate: null,
    student: {
      id: 0,
    },
    dateTo: null, //Data "până la", format identic cu "de la"
    numberValue: null,
    entityAttribute: {
      id: 0,
    },
    value: "NU",
    properties: null,
    dateFrom: "2020-09-01", //Data "de la"
  };

  //restore console
  let iFrame = document.createElement("iframe");
  iFrame.style.display = "none";
  document.body.appendChild(iFrame);
  window.console = iFrame.contentWindow.console;

  const siiirFetch = async (url, body) =>
    fetch(`${url}?_dc=${new Date().getTime()}`, {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/x-www-form-urlencoded",
      },
      body: body,
    });

  const siiirPost = async (url, body) =>
    fetch(`${url}?_dc=${new Date().getTime()}`, {
      method: "post",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
      body: JSON.stringify(body),
    });

  const siiirDelete = async (url, id, body) =>
    fetch(`${url}${id}?_dc=${new Date().getTime()}`, {
      method: "delete",
      credentials: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
      body: JSON.stringify(body),
    });

  const ids = elevIds.split("\n");

  let progressMax = ids.length;
  let progress = 0;

  for (const elevId of ids) {
    //get attribute list
    const attribResult = await siiirFetch(
      listUrl,
      listParamPrefix + elevId + listParamSuffix
    );
    const attribJSON = await attribResult.json();

    const foundAttrib = {};
    for (const attribute of attribJSON.page.content) {
      const attribId = attribute.entityAttribute.id;
      const attribValue = attribute.value;
      //delete any bad attribute (internet=N)
      if (attribId == 1281 && attribValue == "N") {
        console.log("Found bad attribute, deleting");
        await siiirDelete(deleteActionUrl, attribute.id, attribute);
      } else {
        //increase duplicate count
        foundAttrib["" + attribId] = foundAttrib["" + attribId] + 1 || 1;
      }
    }

    for (const attribIdKey of attribIdArray) {
      //check if attribute already exists
      if (!foundAttrib.hasOwnProperty(attribIdKey)) {
        //create any missing attributes
        const payload = attributeTemplate;
        payload.student.id = elevId;
        payload.entityAttribute.id = attribIdKey;
        payload.value = attribValuesToGenerate[attribIdKey].value;
        await siiirPost(actionUrl, payload);
      }
    }

    progress++;
    console.log(`${progress} / ${progressMax}`);
  }
  console.log("DONE!");
})();