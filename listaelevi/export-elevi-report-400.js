(async () => {
  //import XLSX
  const script = document.createElement("script");
  script.src = "https://unpkg.com/xlsx@0.16.7/dist/xlsx.full.min.js";
  script.async = false;
  document.body.appendChild(script);

  script.addEventListener("load", async () => {
    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;

    const maxpages = 4;

    const downloadUrl = "https://www.siiir.edu.ro/siiir/excel/400.jasper";
    const downloadParam = `type=xls&source=StudyFormationStudentManagementSql&schoolYearId=${schoolYearId}&schoolId=-1&locality=&localityId=-1&street=&streetNumber=&postalCode=&phoneNumber=&faxNumber=&schoolName=&countyId=16&internalSchoolId=-1&levelId=-1&propertyFormId=-1&studyFormationTypeId=-1&countyClause=+s2.id_county+%3D+16&localityClause=+1%3D1&internalIdClause=+1%3D1&levelClause=+1%3D1&propertyFormClause=+1%3D1&studyFormationTypeClause=+1%3D1&propertyForm=-1&level=-1&studyFormationType=-1&pagina=`;

    const sheetName = "Export";
    const checkCell = "A6";
    const copyRowStart = 6;
    const copyRowEnd = 10005;

    //restore console
    let iFrame = document.createElement("iframe");
    iFrame.style.display = "none";
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const mainWorkBook = XLSX.utils.book_new();
    const mainSheet = XLSX.utils.aoa_to_sheet([]);

    //for each page
    const pages = [];
    for (let i = 1; i <= maxpages; i++) {
      pages.push(i);
    }

    //download
    const files = await Promise.all(
      pages.map(async (page) => {
        const response = await fetch(downloadUrl, {
          method: "post",
          credentials: "same-origin",
          headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
          },
          body: downloadParam + page,
        });
        return response.arrayBuffer();
      })
    );

    //parse

    files.forEach((file) => {
      const xlsFile = XLSX.read(file,{type:"array"});
      console.log(xlsFile);
      const xlsSheet = xlsFile.Sheets[sheetName];
      //check A6 for value
      const cellValue = xlsSheet[checkCell].v;
      //if we have values copy to final
      if (cellValue) {
        const sheetJSON = XLSX.utils.sheet_to_json(xlsSheet, { skipHeader:true });
        XLSX.utils.sheet_add_json(mainSheet, sheetJSON, { origin: -1,skipHeader:true });
      }
    });

    //save final file
    const dateString = new Date().toISOString().substring(0, 10);
    const fileName = `export_elevi_${dateString}.xlsb`;
    XLSX.utils.book_append_sheet(mainWorkBook, mainSheet, sheetName);
    XLSX.writeFile(mainWorkBook, fileName);
  });
})();