const doSearch = async () => {
  //prefix+codjud+suffix+pagenum
  const mainListUrlPrefix =
    "http://evaluare.edu.ro/Evaluare/CandFromJudAlfa.aspx?Jud=";
  const mainListUrlSuffix = "&PageN=";

  const judete = [
    "AB","AG","AR","B","BC","BH","BN","BR","BT","BV","BZ","CJ","CL","CS","CT","CV","DB","DJ","GJ","GL","GR","HD","HR","IF","IL","IS","MH","MM","MS","NT","OT","PH","SB","SJ","SM","SV","TL","TM","TR","VL","VN","VS"
  ];

  //make a codjud->id mapping
  const idJudete = judete.reduce(
    (acc, item, index) => {
      acc[item] = index + 1;
      return acc;
    },
    {} //start with empty associative array/ object
  );

  //display find dialog
  let code = null;
  while (code === null) {
    code = prompt("Vă rugăm să introduceți codul:","CV9665263");
  }

  //get cod judet from code
  const codJudetRegex = /([A-Z]+)([0-9]+)/;
  const match = codJudetRegex.exec(code.toUpperCase());
  const codJudet = match[1];
  const idElev = parseInt(match[2]);
  const idJudet = idJudete[codJudet];
  //get first page
  const firstPageResult = await fetch(
    mainListUrlPrefix + idJudet + mainListUrlSuffix + "1"
  );

  const firstPageText = await firstPageResult.text();

  const domParser = new DOMParser();

  const firstPageDOM = domParser.parseFromString(firstPageText, "text/html");
  
  //get option elements
  const optionsNodes = firstPageDOM.querySelectorAll("select option");

  const idElevRegex = /[^(]*\([A-Z]+([0-9]+)\)/;
  //scan options for codes
  const nextPage = Array.from(optionsNodes)
  .map(
    item=>{
      const itemText = item.text;
      const idMatch = idElevRegex.exec(itemText);
      const currentPageStartId = parseInt(idMatch[1]);
      const distance = idElev-currentPageStartId;
      console.log("currentPageStartId: "+currentPageStartId+" / idElev: "+idElev+" / distance: "+distance);
      const pageNr = item.value;
      return {
        pageNr,
        distance
      }
    }
    )
  .reduce(
    (acc,item)=>{
      if (item.distance>0 && item.distance<acc.distance){
        acc.pageNr = item.pageNr;
        acc.distance = item.distance;
      }
      return acc;
    },
    {
      pageNr:0,
      distance:1000000
    }
    );

  //analyze nextPage.page();

  //while NOT currentPageCode<code and nextPageCode>code get page closest to code
  //scan current page for code
  //if found, redirect to page
  //if not found, give message
};

doSearch();

