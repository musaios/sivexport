var maxThread = 4;
var countyId = AMS.util.AppConfig.filtersConfig.entity.id;
var schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
var listUrl = "https://www.siiir.edu.ro/siiir/list/sc/maxAuxiliary";
var actionUrl = "https://www.siiir.edu.ro/siiir/management/sc/maxAuxiliary";
var listParamString = "generatorKey=maxAuxiliary_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22school.county.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B"+countyId+"%5D%7D%2C%7B%22property%22%3A%22school.schoolYear.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B"+schoolYearId+"%5D%7D%5D&sort=%5B%7B%22property%22%3A%22order%22%2C%22direction%22%3A%22ASC%22%7D%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A"+schoolYearId+"%7D&page=1&start=0&limit=1000";

var mainDiv = document.createElement('div');
mainDiv.id = 'mainDiv';
mainDiv.style.backgroundColor = 'white';
mainDiv.style.position = 'fixed';
mainDiv.className = 'x-border-box';
mainDiv.style.top = '20px';
mainDiv.style.right = '20px';
mainDiv.style.left = '20px';
mainDiv.style.bottom = '20px';

var subDiv = document.createElement('div');
subDiv.id = 'subDiv';
subDiv.className = '';
subDiv.style.height = '6em';

var tutorialText = document.createTextNode("Intrați în EduSal, Utilitare Școală->Norme Școli. Selectați Dată Stat și aplicați filtrul. Selectați datele din tabelul cu școli începând cu prima cifră din primul cod sirues până la ultimul buton 'Editează' din ultimul rând în tabel. Dați Ctrl+C sau copiere. Dați Ctrl+V sau lipire în caseta de mai jos în această fereastră.");

subDiv.appendChild(tutorialText);

var importButton = document.createElement('a');
importButton.href="#";
importButton.onclick = () => {
	//check copied list
	var copiedList = document.getElementById('edusalData').value.replace(new RegExp(',', 'g'), '.');
	//process copied list
	var copiedArray = copiedList.split("\n");
	var copiedObject = {};
	for (var i = 0; i<copiedArray.length; i++){
		var currentLine = copiedArray[i];
		if (currentLine.length>0){
			var lineArray = currentLine.split("\t\t");
			if (lineArray[0].length>0){
				copiedObject[parseInt(lineArray[0])]=lineArray;
			}
		}
	}
	
	//download unit list
	fetch(listUrl + "?_dc=" + (new Date).getTime(), {
		method: 'post',
		credentials: 'same-origin',
		headers: {
			"Accept": "application/json",
			"Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
			"X-Requested-With": "XMLHttpRequest"
		},
		body: listParamString
	})
		.then((response) => response.json())
		.then((response) => {
			var payload = response.page.content;
			//modify values
			for (var i = 0; i<payload.length; i++){
				var sirues = payload[i].school.siruesCode;
				//payload.school=null;
				if (copiedObject[parseInt(sirues)] != null){
					var o = copiedObject[parseInt(sirues)];
					var didactic = parseFloat(o[3])+parseFloat(o[7])+parseFloat(o[11])+parseFloat(o[15])+parseFloat(o[19]);
					var aux = parseFloat(o[4])+parseFloat(o[8])+parseFloat(o[12])+parseFloat(o[16])+parseFloat(o[20]);
					var nedidactic = parseFloat(o[5])+parseFloat(o[9])+parseFloat(o[13])+parseFloat(o[17])+parseFloat(o[21]);
					var nedidacticISJ = parseFloat(o[6])+parseFloat(o[10])+parseFloat(o[14])+parseFloat(o[18])+parseFloat(o[22]);
					payload[i].maxDidactic = didactic;
					payload[i].maxDidacticAuxiliar = aux;
					payload[i].maxNedidactic = nedidactic + nedidacticISJ;
				}
			}
			//send back payload
			fetch(actionUrl+ "?_dc=" + (new Date).getTime()+"&"+listParamString,{
				method: 'post',
				credentials: 'same-origin',
				headers: {
					"Accept": "application/json",
					"Content-type": "application/json",
					"X-Requested-With": "XMLHttpRequest"
				},
				body: JSON.stringify(payload)
				})
			}
		);
		return false;
	}

var importText = document.createTextNode('Importă');
importButton.appendChild(importText);

var separatorText = document.createTextNode(' / ');

var cancelButton = document.createElement('a');
cancelButton.href="#";
cancelButton.onclick = () => {
	var body = document.getElementsByTagName("body")[0];
	var mainDiv = document.getElementById('mainDiv');
	body.removeChild(mainDiv);
	return false;
}

var cancelText = document.createTextNode('Anulează');
cancelButton.appendChild(cancelText);

subDiv.appendChild(separatorText);
subDiv.appendChild(importButton);
subDiv.appendChild(separatorText);
subDiv.appendChild(cancelButton);

mainDiv.appendChild(subDiv);

var textArea = document.createElement('textarea');
textArea.id = 'edusalData';
textArea.style.width = '100%';
textArea.style.height = '95%';

mainDiv.appendChild(textArea);

var body = document.getElementsByTagName("body")[0];
body.appendChild(mainDiv);
