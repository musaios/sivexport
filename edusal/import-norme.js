//DOES NOT WORK. Event validation blocks POST requests. Try with nodejs/phantomjs
const listURL = "https://edusal.edu.ro/auth/isj/utilitareScoli/NormeScoli.aspx";
const luna = 11;
const an = 2019;
const listParams = `?a=1&luna=${luna}&an=${an}&save=False`;
const listEditParams = `?a=1&luna=${luna}&an=${an}&save=False&id=`;
const listSaveParams = `?a=1&luna=${luna}&an=${an}&save=True&id=`;

const header = [
"Cod şcoală","L",
"Nume şcoală","L",
"Norme aprobate","L",
"MEN-Didactic","L",
"MEN-Auxiliar","L",
"MEN-Nedidactic","L",
"MEN-Nedidactic ISJ","L",
"CL-Didactic","L",
"CL-Auxiliar","L",
"CL-Nedidactic","L",
"CL-Nedidactic ISJ","L",
"CJ-Didactic","L",
"CJ-Auxiliar","L",
"CJ-Nedidactic","L",
"CJ-Nedidactic ISJ","L",
"VP-MEN-Didactic","L",
"VP-MEN-Auxiliar","L",
"VP-MEN-Nedidactic","L",
"VP-MEN-Nedidactic ISJ","L",
"VP-CLCJ-Didactic","L",
"VP-CLCJ-Auxiliar","L",
"VP-CLCJ-Nedidactic","L",
"VP-CLCJ-Nedidactic ISJ","L",
"Buton"
];

let importTemplate = {
"ctl00$phMain$txtFCodScoala": "",
"ctl00$phMain$ddlMonth": an,
"ctl00$phMain$ddlYear": luna,
"ctl00$phMain$txtScoala": "",
"ctl00$phMain$txtNormeAprobate": "0",  //total
"ctl00$phMain$txtNormeDid1": "0",      //MEN
"ctl00$phMain$txtNormeDidAux1": "0",   //MEN
"ctl00$phMain$txtNormeNedid1": "0",    //MEN
"ctl00$phMain$txtNedidacticISJ1": "0", //MEN
"ctl00$phMain$txtNormeDid2": "0",      //CL
"ctl00$phMain$txtNormeDidAux2": "0",   //CL
"ctl00$phMain$txtNormeNedid2": "0",    //CL
"ctl00$phMain$txtNedidacticISJ2": "0", //CL
"ctl00$phMain$txtNormeDid3": "0",      //CJ
"ctl00$phMain$txtNormeDidAux3": "0",   //CJ
"ctl00$phMain$txtNormeNedid3": "0",    //CJ
"ctl00$phMain$txtNedidacticISJ3": "0", //CJ
"ctl00$phMain$txtNormeDid4": "0",      //VPMEN
"ctl00$phMain$txtNormeDidAux4": "0",   //VPMEN
"ctl00$phMain$txtNormeNedid4": "0",    //VPMEN
"ctl00$phMain$txtNedidacticISJ4": "0", //VPMEN
"ctl00$phMain$txtNormeDid5": "0",      //VPCLCJ
"ctl00$phMain$txtNormeDidAux5": "0",   //VPCLCJ
"ctl00$phMain$txtNormeNedid5": "0",    //VPCLCJ
"ctl00$phMain$txtNedidacticISJ5": "0", //VPCLCJ
"__EVENTTARGET": "",
"__EVENTARGUMENT": "",
"__VIEWSTATE": "",
"__VIEWSTATEGENERATOR": "",
"__EVENTVALIDATION": ""
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

const formEncode = obj => {
  let formData = new URLSearchParams();
  for (let prop in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, prop)) {
      formData.append(prop,obj[prop]);
    }
  }
  return formData;
}

const events = [
"__EVENTTARGET",
"__EVENTARGUMENT",
"__VIEWSTATE",
"__VIEWSTATEGENERATOR",
"__EVENTVALIDATION"
];


//init state handlers
let last = {};
events.forEach(item=>last[item]="");


//iife to use async on top level
(async ()=>{

let listResult = await fetch(listURL+listParams);
let listText = await listResult.text();
let listHTML = document.createElement("html");
listHTML.innerHTML = listText;

//save state
events.forEach(item=>last[item]=listHTML.querySelector("#"+item).value);

let statTableRows = listHTML.querySelectorAll("div#tabel_stat_personal>table tr");
let maxRows = statTableRows.length;
let maxCols = header.length;

for (let row = 2; row<maxRows; row++){
  //get code and controlname
  let sirues = statTableRows[row].getElementsByTagName("td")[0].innerText.trim();
  let controlname = statTableRows[row].getElementsByTagName("input")[0].name;
  console.log(`Row: ${sirues}/${controlname}`);
  
  //simulate button click to get ID

  let postBody = { ...importTemplate, ...last }; //create a copy of the template and merge the state
  postBody[controlname+".x"]="40";
  postBody[controlname+".y"]="10";

  let idListResult = await fetch(listURL+listParams, {
                method: 'post',
                headers: {
                  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
                body: formEncode(postBody) //urlencode using URLSearchParams
            });

  console.log("Response headers: ", idListResult.headers);

  let idListText = await idListResult.text();

  let idListHTML = document.createElement('html');
  idListHTML.innerHTML = idListText;
  
  //extract state
  events.forEach(item=>last[item]=idListHTML.querySelector("#"+item).value);

}

console.log("State: ",last);

})();
