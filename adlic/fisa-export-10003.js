//DOES NOT WORK because of server side limitation. but leaving it up for reference
(async ()=>{

const reportUrl = "https://adlic.edu.ro/adlic/reports/10003";

const reportHeaders = {
  "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
  "X-Requested-With": "XMLHttpRequest"
}
const reportFormData = "allFromCenter=true&source=FisaNotePag1";
const reportResponse = await fetch(reportUrl,{
      method: 'post',
      credentials: 'same-origin',
      headers: reportHeaders,
      body: reportFormData
    });
const blob = await reportResponse.blob();
const url = window.URL.createObjectURL(blob);
const a = document.createElement('a');
a.href = url;
a.download = "fise.pdf";
a.click(); 

})();