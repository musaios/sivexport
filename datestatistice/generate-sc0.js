//IIFE for async
(async() => {
    const pageLimit = 500;
    const unlock = true; //set to false if you don't want to unlock
    const generate = true; //set to false if you don't want to generate values
    const lock = true; //set to false if you don't want to lock

    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
    const listUrl = "https://www.siiir.edu.ro/siiir/list/sc0SchoolDTO.json";
    const listParams = `generatorKey=SC0SDQG&filter=%5B%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%2C%22userLevel%22%3A%22COUNTY%22%7D`;

    //NOTE: the "id" is actually school.internalId for locking/unlocking and school.id for SC0 and  and not record id
    const unlockActionUrl = "https://www.siiir.edu.ro/siiir/openSchoolCost.json";
    const generateActionUrl = "https://www.siiir.edu.ro/siiir/generateSc0.json";
    const lockActionUrl = "https://www.siiir.edu.ro/siiir/closeSchoolCost.json";
    
    //NOTE: the param string is identical to all 3 operations only the URL and ID change
    const actionParamsPrefix = `id=`;
    const actionParamsSuffix = `&schoolYearId=${schoolYearId}&configKey=close_sc0_key`
    
    //restore console
    let iFrame = document.createElement('iframe');
    iFrame.style.display = 'none';
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const siiirFetch = async (url, body) => fetch(`${url}?_dc=${new Date().getTime()}`, {
        method: "post",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
        body: body,
    });

    //get nr of results
    const primingListResult = await siiirFetch(listUrl, listParams + "&limit=1&page=1&start=0");

    const primingListJSON = await primingListResult.json();

    const maxItems = primingListJSON.page.total;
    const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

    for (let page = 1; page <= maxPages; page++) {

        const start = (page - 1) * pageLimit;

        console.log(`Processing page ${page} of ${maxPages}`);

        const listResult = await siiirFetch(listUrl, listParams + `&limit=${pageLimit}&page=${page}&start=${start}`);

        const listJSON = await listResult.json();

        //do the actions
        for (const item of listJSON.page.content) {

            //unlock if needed
            if (item.finalized && unlock) await siiirFetch(unlockActionUrl, actionParamsPrefix + item.school.internalId + actionParamsSuffix);
            
            //generate
            if (generate) await siiirFetch(generateActionUrl, actionParamsPrefix + item.school.id + actionParamsSuffix);
            
            //lock
            if (lock) await siiirFetch(lockActionUrl, actionParamsPrefix + item.school.internalId + actionParamsSuffix);

            console.log("Processed: " + item.school.longName);
        }
        console.log("DONE!");
    }
})();