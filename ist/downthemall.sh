#!/bin/bash
#Select county by code. 
JUDET="CV"
#Select past month, by presuming that you're running this around 1-5 of the next month
CURRENT_MONTH=`date +%m%Y  --date="10 days ago"`

echo "Running report $CURRENT_MONTH $JUDET ..."
#cleanup from last run
rm *.csv
rm index.php
#get main list of reports
wget --quiet --no-check-certificate https://37.128.231.251/broadband/reports/index.php
cat index.php | grep -o '/broadband/reports/scoli_down-[0-9]*'$CURRENT_MONTH'\-[0-9]*\.csv' | sed 's/\/b/https:\/\/37.128.231.251\/b/g' > url.txt
#download relevant CSV files
wget --quiet --no-check-certificate -i url.txt

#count files
TOTAL_FILES=`ls scoli_down-??$CURRENT_MONTH-*.csv | wc -l`
echo "$TOTAL_FILES - total rapoarte"
#filter by county
cat scoli_down-??$CURRENT_MONTH-*.csv | grep ",$JUDET," > raport-$CURRENT_MONTH.txt
#sort highest to lowest
sort raport-$CURRENT_MONTH.txt | uniq -c | sort -nr


#example:
#748 - total rapoarte
#    743 CE,CV,Scoala Gimnaziala Vegh Antal Cernat,Cernat,Cernat Judet: Covasna; ,RTC,804,172.31.102.3,FW-NLC-CV-3-Cernat
#    743 CE,CV,Sc Gimn Mathe Janos Herculian,Herculian,Herculian STR Principala,RTC,804,172.31.102.23,FW-NLC-CV-23-Herculian
#    743 CE,CV,Sc Gimn Boloni Farkas Sandor Belin,Belin,Belin STR Principala NR. 378,2K,1204,172.31.103.20,FW-NLC-CV-20-Belin
#    743 CE,CV,Sc Gimn Bartha Karoly Borosneu Mare,Borosneu-Mare,Borosneu-Mare Borosneu Mare STR Principala NR. 394,RTC,804,172.31.102.21,FW-NLC-CV-21-Borosneu-Mare
#    682 CE,CV,Scoala Gimnaziala Kicsi Antal Turia,Turia,Turia STR Principala NR. 833,RTC,804,172.31.102.7,FW-NLC-CV-7-Turia
#...
#      2 CE,CV,Sc Gimn Czetz Janos Ghidfalau,Ghidfalau,Ghidfalau STR Principala NR. 106,2K,1204,172.31.103.22,FW-NLC-CV-22-Ghidfalau
#      1 CE,CV,Scoala Gimnaziala Kalnoky Ludmilla,Valea-Crisului,Valea-Crisului Valea Crisului STR Scolii NR. 154,RTC,804,172.31.102.13,FW-NLC-CV-13-Valea-Crisului
#
#
# the formula: nr/total gives you the percent of downtime. So in the example the first line was 743/748 (99.33%) down.