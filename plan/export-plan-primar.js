//IIFE for async
(async() => {
    const listUrl = "https://www.siiir.edu.ro/siiir/list/primar.json";
    const pageLimit = 1000;
    const schoolYearId = AMS.util.AppConfig.selectedYear.data.id;
    const countyId = 16; //Covasna
    const listParamsPrefix = `generatorKey=primar_GRID_GENERATOR_KEY&filter=%5B%7B%22property%22%3A%22schoolYear.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${schoolYearId}%5D%7D%2C%7B%22property%22%3A%22school.county.id%22%2C%22criteria%22%3A%22EQUAL%22%2C%22parameters%22%3A%5B${countyId}%5D%7D%5D&sort=%5B%5D&filterName=%5B%5D&requestParams=%7B%22schoolYearId%22%3A${schoolYearId}%7D`;
    const headers = [
        ["ID plan", "id"],
        ["Cod unitate PJ", "school.code"],
        ["Nume unitate PJ", "school.longName"],
        ["Cod unitate", "schoolForPlan.code"],
        ["Nume unitate", "schoolForPlan.longName"],
        ["Tip finantare", "fundingForm.code"],
        ["Tip invatamant", "educationType.description"],
        ["Forma de invatamant", "educationForm.description"],
        ["Tip predare", "teachingType.description"],
        ["Mod predare", "teachingMode.description"],
        ["Tipul formatiunii de studiu", "studyFormationType.description"],
        ["Limba", "teachingLanguage.description"],
        ["Grupă specială", "specialGroup.description"],
        ["Deficiențe", "deficiency.description"],
        ["Clase", "classNo"],
        ["Locuri", "studentNo"],
        ["Versiune", "version"],
    ];

    //restore console
    let iFrame = document.createElement('iframe');
    iFrame.style.display = 'none';
    document.body.appendChild(iFrame);
    window.console = iFrame.contentWindow.console;

    const csvData = [];

    //get nr of results
    const primingListResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
        method: "post",
        credentials: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-type": "application/x-www-form-urlencoded",
        },
        body: listParamsPrefix + "&limit=1&page=1&start=0",
    });

    const primingListJSON = await primingListResult.json();

    const maxItems = primingListJSON.page.total;
    const maxPages = Math.ceil(maxItems / pageLimit); //calculate nr of pages

    for (let page = 1; page <= maxPages; page++) {

        const start = (page - 1) * pageLimit;

        console.log(`Downloading page ${page} of ${maxPages}`);

        const listResult = await fetch(listUrl + "?_dc=" + new Date().getTime(), {
            method: "post",
            credentials: "same-origin",
            headers: {
                Accept: "application/json",
                "Content-type": "application/x-www-form-urlencoded",
            },
            body: listParamsPrefix + `&limit=${pageLimit}&page=${page}&start=${start}`,
        });

        const listJSON = await listResult.json();

        const pageCSVData = await Promise.all(
            listJSON.page.content.map(
                async(item, index) => {
                    return headers.map(key => {
                        const keyPath = key[1];
                        try {
                            const value = keyPath.split('.')
                                .reduce((o, i) => o[i], item) //resolve value
                                .toString()
                                .split('"').join('""'); //escape quotes
                            return `"${value}"`; //return the formatted field
                        } catch (err) {
                            return `""`;
                        }
                    }).join(',');
                }
            )
        );
        csvData.push(pageCSVData.join("\n"));
    }

    const dateString = (new Date()).toISOString().substring(0,10);
    const fileName = `export_plan_primar_${dateString}.csv`;
    const buffer = "\uFEFF" + headers.map(h => `"${h[0]}"`).join(",") + "\n" + csvData.join("\n");
    const blob = new Blob([buffer], {
        type: "text/csv;charset=utf8;",
    });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
})();
